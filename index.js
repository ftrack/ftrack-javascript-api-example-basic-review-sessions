'use strict';
(function (ftrack, ftrackWidget) {

    /** Register a helper to format collaborators. */
    Handlebars.registerHelper('collaboratorsFormatter', function(invitees) {
        var names = [];
        for (var index = 0; index < invitees.length; index++) {
            names.push(invitees[index].name);
        }
        return names.join(', ');
    });

    /** Register a helper to format a moment date. */
    Handlebars.registerHelper('dateFormatter', function(date) {
        return date && date.fromNow() || '';
    });

    /** Register a helper to render review session state (open or closed). */
    Handlebars.registerHelper('stateFormatter', function() {
        var reviewSession = this,
            now = new Date();

        return new Handlebars.SafeString(
            (now <= this.end_date && now >= this.start_date) ?
            '<div class="open">Open</div>' : '<div class="closed">Closed</div>'
        );
    });

    /** Register a helper to format review session objects. */
    Handlebars.registerHelper(
        'reviewSessionObjectsFormatter',
        function(reviewSessionObjects) {
            if (reviewSessionObjects.length == 0) {
                return '';
            }

            return new Handlebars.SafeString(
                    '<div style="background-image: url(' +
                        session.thumbnailUrl(
                            reviewSessionObjects[0].asset_version.thumbnail_id,
                            40
                        ) +
                    ')" class="thumbnail"></div>'
            );
        }
    );

    var session = null;

    // Define and compile handlebars template.
    var template = Handlebars.compile([
            '<table class="review-session-table">',
                '<tr>',
                    '<th></th>',
                    '<th></th>',
                    '<th>Name</th>',
                    '<th>Collaborators</th>',
                    '<th>Created</th>',
                    '<th>Latest activity</th>',
                '</tr>',
                '{{#each review_sessions}}',
                    '<tr>',
                        '<td>{{stateFormatter}}</td>',
                        '<td>{{reviewSessionObjectsFormatter review_session_objects}}</td>',
                        '<td>{{name}}</td>',
                        '<td>',
                            '{{collaboratorsFormatter review_session_invitees}}',
                        '</td>',
                        '<td>{{dateFormatter created_at}} by {{created_by.first_name}} {{created_by.last_name}}</td>',
                        '<td>{{dateFormatter latest_activity}}</td>',
                    '</tr>',
                '{{/each}}',
            '</table>'
        ].join(''));

    /** Initialize session with credentials once widget has loaded */
    function onWidgetLoad() {
        var credentials = ftrackWidget.getCredentials();
        session = new ftrack.Session(
            credentials.serverUrl,
            credentials.apiUser,
            credentials.apiKey
        );

        console.debug('Initializing API session.');
        session.initializing.then(function () {
            console.debug('Session initialized');
        });

        onWidgetUpdate();
    }

    /** Query API for review sessions when widget has loaded. */
    function onWidgetUpdate() {
        var entity = ftrackWidget.getEntity();
        if (entity.type !== 'Project') {
            renderWidget('Cannot display review session on this object.');
            return;
        }

        renderWidget('Loading...');

        console.debug('Querying new data for entity', entity);

        // Query review session data.
        var reviewSessionRequest = session.query(
            'select name, start_date, end_date, created_by.first_name, ' +
            'created_by.last_name, created_at, review_session_invitees.name, ' +
            'review_session_objects.asset_version.thumbnail_id, ' +
            'review_session_objects.statuses.created_at from ' +
            'ReviewSession where project_id is "' + entity.id +
            '" order by created_at desc'
        );

        var scope = {};
        scope.data = [];
        scope.reviewSessionObjectIds = [],
        scope.reviewSessionObjectMap = {};

        // Chain requests and gather results.
        reviewSessionRequest.then(function (result) {
            console.debug('Query result', result);
            var data = result.data;
            var sessionIndex = 0;

            // Loop all review sessions.
            data.forEach(function(reviewSession) {
                // Loop all review session objects.
                reviewSession.review_session_objects.forEach(
                    function(reviewSessionObject) {
                        scope.reviewSessionObjectIds.push(
                            reviewSessionObject.id
                        );
                        scope.reviewSessionObjectMap[reviewSessionObject.id] = (
                            reviewSession
                        );

                        var statusIndex = 0;

                        // Loop all statuses on the review session objects.
                        reviewSessionObject.statuses.forEach(
                            function(reviewSessionStatus) {
                                var createdAt = reviewSessionStatus.created_at;
                                // Update activity.
                                if (
                                    !reviewSession.latest_activity ||
                                    reviewSession.latest_activity < createdAt
                                ) {
                                    reviewSession.latest_activity = createdAt;
                                }
                        });
                    }
                );
            });

            scope.data = data;

            if (scope.reviewSessionObjectIds.length > 0) {
                // Query for notes and return new Promise.
                var notesQuery = session.query(
                    'select date, parent_id from Note where parent_id in (' +
                        scope.reviewSessionObjectIds.join(',') +
                    ')'
                );
                return notesQuery;
            } else {
                // Resolve empty result promise.
                return Promise.resolve({data: []});
            }
        }).then(function(result) {
            var notes = result.data;

            // Loop all notes.
            notes.forEach(function(note) {
                var reviewSession = scope.reviewSessionObjectMap[note.parent_id];
                if (
                    !reviewSession.latest_activity ||
                    reviewSession.latest_activity < note.date
                ) {
                    reviewSession.latest_activity = note.date;
                }
            });

            renderWidget(scope.data);
        });
    }

    /** Update widget UI and display *data*. */
    function renderWidget(data) {
        var widgetEl = document.getElementById('widget');
        if (data.constructor === String) {
            widgetEl.innerHTML = data;
        } else {
            widgetEl.innerHTML = template(
                {
                    review_sessions: data
                }
            );            
        }
    }

    /** Initialize widget once DOM has loaded. */
    function onDomContentLoaded() {
        console.debug('DOM content loaded, initializing widget.');
        ftrackWidget.initialize({
            onWidgetLoad: onWidgetLoad,
            onWidgetUpdate: onWidgetUpdate,
        });
    }

    window.addEventListener('DOMContentLoaded', onDomContentLoaded);
}(window.ftrack, window.ftrackWidget));