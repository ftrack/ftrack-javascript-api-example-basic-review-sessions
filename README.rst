
*********************************************
ftrack JavaScript API: Review Sessions Widget
*********************************************

This is a basic example showing how to use the templating engine library,
`Handlebars <http://handlebarsjs.com/>`_, for widgets in ftrack. The example
shows review sessions for a project.

For more information on the APIs used, please refer to the documentation:

* `Building dashboard widgets <http://ftrack.rtd.ftrack.com/en/stable/developing/building_dashboard_widgets.html>`_
* `JavaScript API client <http://ftrack-javascript-api.rtd.ftrack.com/en/stable/>`_